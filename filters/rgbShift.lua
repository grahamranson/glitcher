--https://www.shadertoy.com/view/4t23Rc
local kernel =
{
	category = "composite",
	group = "glitcher",
	name = "rgbShift",
	isTimeDependent = true,
	vertexData =
	{
		{
			name = "speed",
			default = 5.0,
			min = 0.0,
			max = 100.0,
			index = 0,  -- This corresponds to "CoronaVertexUserData.x"
		},
		{
			name = "amplitude",
			default = 0,
			min = 0.0,
			max = 1,
			index = 1,  -- This corresponds to "CoronaVertexUserData.y"
		},
	},

	fragment = [[

		P_UV vec4 rgbShift( in vec2 p , in vec4 shift) {
			shift *= 2.0*shift.w - 1.0;
			P_UV vec2 rs = vec2(shift.x,-shift.y);
			P_UV vec2 gs = vec2(shift.y,-shift.z);
			P_UV vec2 bs = vec2(shift.z,-shift.x);

			P_DEFAULT float r = texture2D(CoronaSampler0, p+rs, 0.0).x;
			P_DEFAULT float g = texture2D(CoronaSampler0, p+gs, 0.0).y;
			P_DEFAULT float b = texture2D(CoronaSampler0, p+bs, 0.0).z;

			return vec4(r,g,b,1.0);
		}

		P_UV vec4 noise( in vec2 p ) {
			return texture2D(CoronaSampler1, p, 0.0);
		}

		P_UV vec4 vec4pow( in vec4 v, in float p ) {
			// Don't touch alpha (w), we use it to choose the direction of the shift
			// and we don't want it to go in one direction more often than the other
			return vec4(pow(v.x,p),pow(v.y,p),pow(v.z,p),v.w);
		}

		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ){

			P_UV vec2 p = texCoord;
			P_UV vec4 c = vec4(0.0,0.0,0.0,1.0);

			// Elevating shift values to some high power (between 8 and 16 looks good)
			// helps make the stuttering look more sudden
			P_UV vec4 shift = vec4pow(noise(vec2(CoronaVertexUserData.x*CoronaTotalTime,2.0*CoronaVertexUserData.x*CoronaTotalTime/25.0 )),8.0)
						*vec4(CoronaVertexUserData.y,CoronaVertexUserData.y,CoronaVertexUserData.y,1.0);;

			c += rgbShift(p, shift);

			P_COLOR vec4 col = c;

			return CoronaColorScale(col);
		}
	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	--self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	display.setDefault( "textureWrapX", "repeat" )
	display.setDefault( "textureWrapY", "repeat" )

	display.setDefault( "magTextureFilter", "nearest" )
	display.setDefault( "minTextureFilter", "nearest" )
	
	local imageTexture = graphics.newTexture( { type = "image", filename = "glitcher/textures/rgbShift.png", baseDir=system.ResourceDirectory } )

	display.setDefault( "textureWrapX", "clampToEdge" )
	display.setDefault( "textureWrapY", "clampToEdge" )

	self.view.fill =
	{
		type = "composite",
		paint1 = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir },
		paint2 = { type = "image", filename = imageTexture.filename, baseDir = imageTexture.baseDir }
	}

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
