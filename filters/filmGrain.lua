--https://www.shadertoy.com/view/4sXSWs
local kernel =
{
	category = "filter",
	group = "glitcher",
	name = "filmGrain",
	isTimeDependent = true,
	vertexData =
	{
		{
			name = "strength",
			default = 1.0,
			min = 0,
			max = 50.0,
			index = 0,  -- This corresponds to "CoronaVertexUserData.x"
		},
	},

	fragment = [[

		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ){


			P_UV vec2 uv = texCoord;

			P_UV vec4 color = texture2D(CoronaSampler0, uv);

			P_DEFAULT float strength = 16.0 * CoronaVertexUserData.x;

			P_DEFAULT float x = (uv.x + 4.0 ) * (uv.y + 4.0 ) * (CoronaTotalTime * 10.0);
			P_UV vec4 grain = vec4(mod((mod(x, 13.0) + 1.0) * (mod(x, 123.0) + 1.0), 0.01)-0.005) * strength;

			P_COLOR vec4 col;

			grain = 1.0 - grain;
			col = color * grain;

			return CoronaColorScale(col);
		}

	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
