local Glitcher = {}
Glitcher.__index = Glitcher

local lfs = require( "lfs" )

--- Creates a new Glitch shader object.
-- @param params The paramaters for the shader.
-- @return The Glitch shader.
function Glitcher:new( params )

	local instance = setmetatable( {}, self )

	self._filters = {}

	--[[
	for file in lfs.dir( system.pathForFile( "glitcher/filters", system.ResourceDirectory ) ) do
		if file ~= "." and file ~= ".." then
			local name = string.gsub( file, ".lua", "" )
			self._filters[ name ] = require( "glitcher.filters." .. name )
		end
	end
	--]]

	local loadFilter = function( name )
		self._filters[ name ] = require( "glitcher.filters." .. name )
	end

	loadFilter( "badConnection" )
	loadFilter( "badReception" )
	loadFilter( "badTV" )
	loadFilter( "blank" )
	loadFilter( "chromaticAberration" )
	loadFilter( "filmGrain" )
	loadFilter( "melt" )
	loadFilter( "mpegArtifacts" )
	loadFilter( "rgbShift" )
	loadFilter( "shake" )
	loadFilter( "shudder" )
	loadFilter( "smear" )
	loadFilter( "vhsSquiggles" )
	loadFilter( "wobble" )

	self._enabled = {}

	local centreX = display.screenOriginX + display.actualContentWidth * 0.5
	local centreY = display.screenOriginY + display.actualContentHeight * 0.5

	self._contentGroup = params.content
	self._contentGroup.parent0 = self._contentGroup.parent
	self._canvas = graphics.newTexture( { type = "canvas", width = display.actualContentWidth, height = display.actualContentHeight } )
	self._canvas.anchorX, self._canvas.anchorY = -( centreX / display.actualContentWidth ), -( centreY / display.actualContentHeight )
	self._canvas:draw( self._contentGroup )

	-- create a rectangle for the output
	self._effectGroup = display.newGroup()

	if params.onTouch then
		self._effectGroup:addEventListener( "touch", params.onTouch )
	end

	if params.onTap then
		self._effectGroup:addEventListener( "tap", params.onTap )
	end

	Runtime:addEventListener( "enterFrame", self )

	self:enable( "blank" )

	return instance

end

--- Enables a filter.
-- @param name The name of the filter.
-- @param params Table of properties to apply to the filter, optional.
function Glitcher:enable( name, params )

	self:disable( name )

	if self._filters and self._filters[ name ] then

		-- calculate the true centre of the screen
		local centreX = display.screenOriginX + display.actualContentWidth * 0.5
		local centreY = display.screenOriginY + display.actualContentHeight * 0.5

		if self._effectGroup and self._canvas and self._canvas.filename and self._canvas.baseDir then

			-- create the view for the effect
			local view = display.newImageRect( self._effectGroup, self._canvas.filename, self._canvas.baseDir, display.actualContentWidth, display.actualContentHeight )

			-- centre the view
			view:translate( centreX, centreY )

			-- create the new filter
			self._enabled[ name ] = self._filters[ name ]:new( view,
				{
					canvas = self._canvas
				}
			)

			-- loop through any passed in properties and apply them
			for k, v in pairs( params or {} ) do
				self:set( name, k, v )
			end

			-- Add this view to the canvas
			if name ~= "blank" then
				self._canvas:draw( view )
			end

		end

	end

end

--- Disables a filter.
-- @param name The name of the filter.
function Glitcher:disable( name )

	if self._enabled[ name ] then

		display.remove( self._enabled[ name ].view )
		self._enabled[ name ].view = nil

		self._enabled[ name ] = nil

		local contentParent = self._contentGroup.parent0 or display.getCurrentStage()
		if contentParent and contentParent.insert then
			contentParent:insert( self._contentGroup )
		end

	end

end

--- Checks if a filter is currently enabled.
-- @param name The name of the filter.
-- @return True if it is, false otherwise.
function Glitcher:enabled( name )
	return self._enabled[ name ] ~= nil
end

--- Gets the view for all the effects.
-- @return The view object.
function Glitcher:getView()
	return self._effectGroup
end

function Glitcher:getAvailableFilters()
	local filters = {}
	for k, _ in pairs( self._filters ) do
		if k ~= "blank" then
			filters[ #filters + 1 ] = k
		end
	end
	return filters
end

--- Sets a filter property.
-- @param name The name of the filter.
-- @param key The name of the property.
-- @param value The new value for the property.
function Glitcher:set( name, key, value )
	if self._enabled[ name ] then
		self._enabled[ name ].view.fill.effect[ key ] = value
	end
end

--- Gets a filter property.
-- @param name The name of the filter.
-- @param key The name of the property.
-- @return The property value.
function Glitcher:get( name, key )
	if self._enabled[ name ] then
		return self._enabled[ name ].view.fill.effect[ key ]
	end
end

--- Increments a filter property.
-- @param name The name of the filter.
-- @param key The name of the property.
-- @param amount The amount to increment the value by.
function Glitcher:increment( name, key, amount )
	if self._enabled[ name ] and self:get( name, key ) then
		self:set( name, key, self:get( name, key ) + ( amount or 0 ) )
	end
end

--- Decrements a filter property.
-- @param name The name of the filter.
-- @param key The name of the property.
-- @param amount The amount to decrement the value by.
function Glitcher:decrement( name, key, amount )
	self:increment( name, key, -amount )
end


--- Invalidates the canvas object. Used internally.
function Glitcher:_invalidate()
	if self._canvas and self._canvas[ "invalidate" ] then
		self._canvas:invalidate( "cache" )
	end
end

--- Update handler, used internally when isStatic is false.
-- @param event Event table for the listener.
function Glitcher:enterFrame( event )
	self:_invalidate()
end

--- Destroys this Glitch object.
function Glitcher:destroy()

	Runtime:removeEventListener( "enterFrame", self )

	for k, v in pairs( self._enabled ) do
		self:disable( k )
	end
	self._enabled = nil

	display.remove( self._effectGroup )
	self._effectGroup = nil

	local contentParent = self._contentGroup.parent0 or display.getCurrentStage()
	if contentParent and contentParent.insert then
		contentParent:insert( self._contentGroup )
	end

	self._contentGroup.parent0 = nil

	self._canvas:releaseSelf()
	self._canvas = nil

	local contentGroup = self._contentGroup
	self._contentGroup = nil

	return contentGroup

end

return Glitcher
