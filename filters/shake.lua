--https://www.shadertoy.com/view/wsBXWW
local kernel =
{
	category = "filter",
	group = "glitcher",
	name = "shake",
	isTimeDependent = true,
	vertexData =
	{
		{
			name = "amount",
			default = 0.1,
			min = 0,
			max = 1,
			index = 0,  -- This corresponds to "CoronaVertexUserData.x"
		},
		{
			name = "speed",
			default = 3.0,
			min = 0,
			max = 10.0,
			index = 1,  -- This corresponds to "CoronaVertexUserData.y"
		},
	},

	fragment = [[

		P_UV vec3 random3(vec3 c) {
			P_DEFAULT float j = 4096.0*sin(dot(c,vec3(17.0, 59.4, 15.0)));
			P_UV vec3 r;
			r.z = fract(512.0*j);
			j *= .125;
			r.x = fract(512.0*j);
			j *= .125;
			r.y = fract(512.0*j);
			return r;
		}
		const P_DEFAULT float F3 =  0.3333333;
		const P_DEFAULT float G3 =  0.1666667;

		P_DEFAULT float simplex3d(vec3 p) {
			 P_UV vec3 s = floor(p + dot(p, vec3(F3)));
			 P_UV vec3 x = p - s + dot(s, vec3(G3));

			 P_UV vec3 e = step(vec3(0.0), x - x.yzx);
			 P_UV vec3 i1 = e*(1.0 - e.zxy);
			 P_UV vec3 i2 = 1.0 - e.zxy*(1.0 - e);

			 P_UV vec3 x1 = x - i1 + G3;
			 P_UV vec3 x2 = x - i2 + 2.0*G3;
			 P_UV vec3 x3 = x - 1.0 + 3.0*G3;

			 P_UV vec4 w, d;

			 w.x = dot(x, x);
			 w.y = dot(x1, x1);
			 w.z = dot(x2, x2);
			 w.w = dot(x3, x3);

			 w = max(0.6 - w, 0.0);

			 d.x = dot(random3(s)-.5, x);
			 d.y = dot(random3(s + i1)-.5, x1);
			 d.z = dot(random3(s + i2)-.5, x2);
			 d.w = dot(random3(s + 1.0)-.5, x3);

			 w *= w;
			 w *= w;
			 d *= w;

			 return dot(d, vec4(52.0));
		}

		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ){

			P_UV vec2 uv = texCoord;
			P_UV vec3 p3 = vec3(0,0, CoronaTotalTime*CoronaVertexUserData.y)*8.0+8.0;
			P_UV vec2 noise = vec2(simplex3d(p3),simplex3d(p3+10.));
			P_COLOR vec4 col = vec4( texture2D( CoronaSampler0, uv+noise*CoronaVertexUserData.x*0.1 ).rgb, 1.0);

			return CoronaColorScale(col);
		}
	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
