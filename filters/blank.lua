local kernel =
{
	category = "filter",
	group = "glitcher",
	name = "blank",
	isTimeDependent = true,
	fragment = [[

		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ) {
			return CoronaColorScale( texture2D( CoronaSampler0, texCoord ) );
		}
	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
