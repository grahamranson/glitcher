--https://www.shadertoy.com/view/MlXfz8
local kernel =
{
	category = "filter",
	group = "glitcher",
	name = "chromaticAberration",
	isTimeDependent = true,
	vertexData =
	{
		{
			name = "speed",
			default = 2.5,
			min = 0.0,
			max = 100.0,
			index = 0,  -- This corresponds to "CoronaVertexUserData.x"
		},
		{
			name = "amount",
			default = 0.035,
			min = 0.0,
			max = 1.0,
			index = 1,  -- This corresponds to "CoronaVertexUserData.y"
		},
	},

	fragment = [[

		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ){


			P_UV vec2 uv = texCoord;
			P_UV vec2 uvRed = uv;
			P_UV vec2 uvBlue = uv;
			P_DEFAULT float s = abs(sin(CoronaTotalTime * CoronaVertexUserData.x)) * CoronaVertexUserData.y;
			uvRed.x += s;
			uvBlue.x -= s;

			P_COLOR vec4 col =  texture2D(CoronaSampler0, uv);

			col.r = texture2D(CoronaSampler0, uvRed).r;
			col.b = texture2D(CoronaSampler0, uvBlue).b;

			return CoronaColorScale(col);
			}
	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
