--https://www.shadertoy.com/view/Md2GDw
local kernel =
{
	category = "composite",
	group = "glitcher",
	name = "mpegArtifacts",
	isTimeDependent = true,
	fragment = [[

		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ) {

			P_UV vec2 uv = texCoord.xy;
			P_UV vec2 block = floor( texCoord.xy / vec2( 16 ) );
			P_UV vec2 uv_noise = block / vec2( 64 );
			uv_noise += floor( vec2( CoronaTotalTime ) * vec2( 1234.0, 3543.0 ) ) / vec2( 64 );

			P_DEFAULT float block_thresh = pow(fract( CoronaTotalTime * 1236.0453), 2.0) * 0.2;
			P_DEFAULT float line_thresh = pow(fract( CoronaTotalTime * 2236.0453), 3.0) * 0.7;

			P_UV vec2 uv_r = uv, uv_g = uv, uv_b = uv;

			// glitch some blocks and lines
			if (texture2D (CoronaSampler1 , uv_noise).r < block_thresh ||
				texture2D (CoronaSampler1 , vec2(uv_noise.y, 0.0)).g < line_thresh) {

				P_UV vec2 dist = (fract(uv_noise) - 0.5) * 0.3;
				uv_r += dist * 0.1;
				uv_g += dist * 0.2;
				uv_b += dist * 0.125;
			}


			P_COLOR vec4 col;
			col.r = texture2D(CoronaSampler0, uv_r).r;
			col.g = texture2D(CoronaSampler0, uv_g).g;
			col.b = texture2D(CoronaSampler0, uv_b).b;

			// loose luma for some blocks
			if (texture2D(CoronaSampler1, uv_noise).g < block_thresh)
				col.rgb = col.ggg;

			// discolor block lines
			if (texture2D(CoronaSampler1, vec2(uv_noise.y, 0.0)).b * 3.5 < line_thresh)
				col.rgb = vec3(0.0, dot(col.rgb, vec3(1.0)), 0.0);

			// interleave lines in some blocks
			if (texture2D(CoronaSampler1, uv_noise).g * 1.5 < block_thresh ||
				texture2D(CoronaSampler1, vec2(uv_noise.y, 0.0)).g * 2.5 < line_thresh) {
				P_DEFAULT float line = fract(texCoord.y / 3.0);
				P_UV vec3 mask = vec3(3.0, 0.0, 0.0);
				if (line > 0.333)
					mask = vec3(0.0, 3.0, 0.0);
				if (line > 0.666)
					mask = vec3(0.0, 0.0, 3.0);

				col.xyz *= mask;
			}

			return CoronaColorScale( col );
		}

	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	display.setDefault( "textureWrapX", "repeat" )
	display.setDefault( "textureWrapY", "repeat" )

	display.setDefault( "magTextureFilter", "linear" )
	display.setDefault( "minTextureFilter", "linear" )

	local imageTexture = graphics.newTexture( { type = "image", filename = "glitcher/textures/rgbShift.png", baseDir=system.ResourceDirectory } )

	display.setDefault( "textureWrapX", "clampToEdge" )
	display.setDefault( "textureWrapY", "clampToEdge" )

    self.view.fill =
	{
		type = "composite",
		paint1 = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir },
		paint2 = { type = "image", filename = imageTexture.filename, baseDir = imageTexture.baseDir }
	}

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
