local kernel =
{
	category = "filter",
	group = "glitcher",
	name = "smear",
	isTimeDependent = true,
	fragment = [[

		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ){

			P_DEFAULT float pyt=3.1415926*2./3.;
			P_DEFAULT float m=-1e10;//very negitive start value for maximisation algorithm.
			P_UV vec4 mv= vec4(0.);//lank starting value of max so far

			P_UV vec2 xy = texCoord;
			int ic=0;//stores smear distance
			for (int i=0;i<30;i++){
				//point offset on a circle
				P_UV vec2 np=vec2(xy.x+float(i)*sin(CoronaTotalTime),xy.y+float(i)*cos(CoronaTotalTime));
				//colour cycles faster than position
				P_DEFAULT float jTime = CoronaTotalTime*1.618;
				//get neerby point
				P_UV vec4 tk=texture2D(CoronaSampler0,np);
				// and if its colourfull enough, use that
				P_DEFAULT float t=tk.r*sin(jTime)+tk.g*sin(jTime+pyt)+tk.b*sin(jTime+2.*pyt)-.01*float(i);
				if (t>m){m=t; mv=tk;ic=i;}
			}
			//mix smeared with background depending ondistance
			P_DEFAULT float sc=float(ic)/30.;
			P_UV vec4 tk=texture2D(CoronaSampler0,xy);
			mv=sc*tk+(1.-sc)*mv;
			P_COLOR vec4 col = vec4(mv.r,mv.g,mv.b,1.0);

			return CoronaColorScale(col);
		}
	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
