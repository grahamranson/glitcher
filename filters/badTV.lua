--https://www.shadertoy.com/view/Md3SRM
local kernel =
{
	category = "filter",
	group = "glitcher",
	name = "badTV",
	isTimeDependent = true,
	vertexData =
	{
		{
			name = "thickDistort",
			default = 1.2,
			min = 0.1,
			max = 6.0,
			index = 0,  -- This corresponds to "CoronaVertexUserData.x"
		},
		{
			name = "fineDistort",
			default = 0.4,
			min = 0.1,
			max = 6.0,
			index = 1,  -- This corresponds to "CoronaVertexUserData.y"
		},
		{
			name = "rollSpeed",
			default = 1,
			min = 0,
			max = 3,
			index = 2,  -- This corresponds to "CoronaVertexUserData.z"
		},
	},
	fragment = [[

		P_DEFAULT float hash(vec2 p) {
			P_DEFAULT float h = dot(p,vec2(127.1,311.7));
			return -1.0 + 2.0*fract(sin(h)*43758.5453123);
			}

			P_DEFAULT float noise(vec2 p) {
			P_UV vec2 i = floor(p);
			P_UV vec2 f = fract(p);

			P_UV vec2 u = f*f*(3.0-2.0*f);

			return mix(mix(hash( i + vec2(0.0,0.0) ),
				hash( i + vec2(1.0,0.0) ), u.x),
			mix( hash( i + vec2(0.0,1.0) ),
				hash( i + vec2(1.0,1.0) ), u.x), u.y);
			}

			P_DEFAULT float noise(vec2 p, int oct) {
			P_DEFAULT mat2 m = mat2( 1.6,  1.2, -1.2,  1.6 );
			P_DEFAULT float f  = 0.0;

			for(int i = 1; i < 3; i++){
				P_DEFAULT float mul = 1.0/pow(2.0, float(i));
				f += mul*noise(p);
				p = m*p;
			}

			return f;
			}

			P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ){

			P_UV vec2 uv = texCoord;

			P_DEFAULT float glitch = pow(cos(CoronaTotalTime*0.5)*1.2+1.0, 1.2);


			if(noise(CoronaTotalTime+vec2(0, 0))*glitch > 0.62){
				uv.y = mod(uv.y+noise(vec2(CoronaTotalTime*4.0, 0)), 1.0);
			}


			P_UV vec2 hp = vec2(0.0, uv.y);
			P_DEFAULT float nh = noise(hp*7.0+CoronaTotalTime*10.0, 3) * (noise(hp+CoronaTotalTime*0.3)*0.8);
			nh += noise(hp*100.0+CoronaTotalTime*10.0, 3)*0.02;
			P_DEFAULT float rnd = 0.0;
			if(glitch > 0.0){
				rnd = hash(uv);
				if(glitch < 1.0){
					rnd *= glitch;
				}
			}
			nh *= glitch + rnd;
			P_DEFAULT float r = texture2D(CoronaSampler0, uv+vec2(nh, 0.08)*nh).r;
			P_DEFAULT float g = texture2D(CoronaSampler0, uv+vec2(nh-0.07, 0.0)*nh).g;
			P_DEFAULT float b = texture2D(CoronaSampler0, uv+vec2(nh, 0.0)*nh).b;

			P_UV vec3 col = vec3(r, g, b);


			P_COLOR vec4 ret = vec4(col.rgb, 1.0);


			return CoronaColorScale(ret);
			}
	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
