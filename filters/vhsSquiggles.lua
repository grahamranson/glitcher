--https://www.shadertoy.com/view/ltSSWV
local kernel =
{
	category = "composite",
	group = "glitcher",
	name = "vhsSquiggles",
	isTimeDependent = true,
	vertexData =
	{
		{
			name = "split",
			default = 1,
			min = 0.1,
			max = 2,
			index = 0,  -- This corresponds to "CoronaVertexUserData.x"
		},
		{
			name = "fault",
			default = 1,
			min = 0.1,
			max = 2,
			index = 1,  -- This corresponds to "CoronaVertexUserData.y"
		},
	},
	fragment = [[

        #define pi 3.1415926

        P_DEFAULT float t;

        P_UV vec3 colorSplit(vec2 uv, vec2 s)
        {
            P_UV vec3 color;
			s = s * CoronaVertexUserData.x;
            color.r = texture2D(CoronaSampler0, uv - s).r;
            color.g = texture2D(CoronaSampler0, uv    ).g;
            color.b = texture2D(CoronaSampler0, uv + s).b;
            return color;
        }

        P_UV vec2 interlace(vec2 uv, float s)
        {
            uv.x += s * (4.0 * fract((uv.y) / 2.0) - 1.0);
            return uv;
        }

        P_UV vec2 fault(vec2 uv, float s)
        {
			s = s * CoronaVertexUserData.y;
            //float v = (0.5 + 0.5 * cos(2.0 * pi * uv.y)) * (2.0 * uv.y - 1.0);
            P_DEFAULT float v = pow(0.5 - 0.5 * cos(2.0 * pi * uv.y), 100.0) * sin(2.0 * pi * uv.y);
            uv.x += v * s;
            return uv;
        }

        P_UV vec2 rnd(vec2 uv, float s)
        {
            uv.x += s * (2.0 * texture2D(CoronaSampler1, uv * 0.05).x - 1.0);
            return uv;
        }

        P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord )
        {
            // t = fract(iTime / 10.0) * 10.0
            P_DEFAULT float t = CoronaTotalTime / 10.0;

            P_UV vec2 uv = texCoord;

            //float s = pow(0.5 + 0.5 * cos(2.0 * pi * t), 1000.0);
            P_DEFAULT float s = texture2D(CoronaSampler1, vec2(t * 0.2, 0.5)).r;

            uv = interlace(uv, s * 0.005);
            //uv = fault(uv, s);
            P_DEFAULT float r = texture2D(CoronaSampler1, vec2(t, 0.0)).x;
            //uv = fault(uv + vec2(0.0, fract(t * 20.0)), r) - vec2(0.0, fract(t * 20.0));
            uv = fault(uv + vec2(0.0, fract(t * 2.0)), 5.0 * sign(r) * pow(abs(r), 5.0)) - vec2(0.0, fract(t * 2.0));
            uv = rnd(uv, s * 0.02);

            P_UV vec3 color = colorSplit(uv, vec2(s * 0.02, 0.0));
            //vec2 m = texture(iChannel2, uv).xy;
            color = mix(color, texture2D(CoronaSampler1, 0.5 * uv + t * 100.0).rgb, 0.25);

            P_COLOR vec4 col;

            col = vec4(color, 1.0);

            return CoronaColorScale( col );
        }

	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	display.setDefault( "textureWrapX", "clampToEdge" )
	display.setDefault( "textureWrapY", "clampToEdge" )

	local imageTexture = graphics.newTexture( { type = "image", filename = "glitcher/textures/vhsSquiggles.png", baseDir=system.ResourceDirectory } )

	display.setDefault( "textureWrapX", "clampToEdge" )
	display.setDefault( "textureWrapY", "clampToEdge" )

    self.view.fill =
	{
		type = "composite",
		paint1 = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir },
		paint2 = { type = "image", filename = imageTexture.filename, baseDir = imageTexture.baseDir }
	}

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
