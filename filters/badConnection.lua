-- https://www.shadertoy.com/view/4syfRt
local kernel =
{
	category = "composite",
	group = "glitcher",
	name = "badConnection",
	isTimeDependent = true,
	vertexData =
	{
		{
			name = "rgb",
			default = 0.1,
			min = 0.0,
			max = 1.0,
			index = 0,  -- This corresponds to "CoronaVertexUserData.x"
		},
		{
			name = "displace",
			default = 0.2,
			min = 0.0,
			max = 1.0,
			index = 1,  -- This corresponds to "CoronaVertexUserData.y"
		},
		{
			name = "interlace",
			default = 0.01,
			min = 0.0,
			max = 1.0,
			index = 2,  -- This corresponds to "CoronaVertexUserData.z"
		},
		{
			name = "dropout",
			default = 0.1,
			min = 0.0,
			max = 1.0,
			index = 3,  -- This corresponds to "CoronaVertexUserData.w"
		},
	},

	fragment = [[

		P_DEFAULT float rand(float n){return fract(sin(n) * 43758.5453123);}

			P_DEFAULT float noise(float p){
				P_DEFAULT float fl = floor(p);
			P_DEFAULT float fc = fract(p);
			return mix(rand(fl), rand(fl + 1.0), fc);
		}

		P_DEFAULT float blockyNoise(vec2 uv, float threshold, float scale, float seed)
		{
			P_DEFAULT float scroll = floor(CoronaTotalTime + sin(11.0 *  CoronaTotalTime) + sin(CoronaTotalTime) ) * 0.77;
			P_UV vec2 noiseUV = uv.yy / scale + scroll;
			P_DEFAULT float noise2 = texture2D(CoronaSampler1, noiseUV).r;

			P_DEFAULT float id = floor( noise2 * 20.0);
			id = noise(id + seed) - 0.5;


			if ( abs(id) > threshold )
				id = 0.0;

			return id;
		}

		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ){

			P_DEFAULT float rgbIntesnsity = CoronaVertexUserData.x + 0.1 * sin(CoronaTotalTime* 3.7);
			P_DEFAULT float displaceIntesnsity = CoronaVertexUserData.y +  0.3 * pow( sin(CoronaTotalTime * 1.2), 5.0);
			P_DEFAULT float interlaceIntesnsity = CoronaVertexUserData.z;
			P_DEFAULT float dropoutIntensity = CoronaVertexUserData.w;


			P_UV vec2 uv = texCoord;

			P_DEFAULT float displace = blockyNoise(uv + vec2(uv.y, 0.0), displaceIntesnsity, 25.0, 66.6);
			displace *= blockyNoise(uv.yx + vec2(0.0, uv.x), displaceIntesnsity, 111.0, 13.7);

			uv.x += displace ;

			P_UV vec2 offs = 0.1 * vec2(blockyNoise(uv.xy + vec2(uv.y, 0.0), rgbIntesnsity, 65.0, 341.0), 0.0);

			P_DEFAULT float colr = texture2D(CoronaSampler0, uv-offs).r;
			P_DEFAULT float colg = texture2D(CoronaSampler0, uv).g;
			P_DEFAULT float colb = texture2D(CoronaSampler0, uv +offs).b;


			P_DEFAULT float line = fract(texCoord.y / 3.0);
			P_UV vec3 mask = vec3(3.0, 0.0, 0.0);
				if (line > 0.333)
					mask = vec3(0.0, 3.0, 0.0);
				if (line > 0.666)
					mask = vec3(0.0, 0.0, 3.0);


			P_DEFAULT float maskNoise = blockyNoise(uv, interlaceIntesnsity, 90.0, CoronaTotalTime) * max(displace, offs.x);

			maskNoise = 1.0 - maskNoise;
			if ( maskNoise == 1.0)
				mask = vec3(1.0);

			P_DEFAULT float dropout = blockyNoise(uv, dropoutIntensity, 11.0, CoronaTotalTime) * blockyNoise(uv.yx, dropoutIntensity, 90.0, CoronaTotalTime);

			mask *= (1.0 - 5.0 * dropout);

			P_COLOR vec4 col = vec4(mask * vec3(colr, colg, colb), 1.0);

			return CoronaColorScale( col );

		}
	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	--self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	display.setDefault( "textureWrapX", "repeat" )
	display.setDefault( "textureWrapY", "repeat" )

	display.setDefault( "magTextureFilter", "linear" )
	display.setDefault( "minTextureFilter", "linear" )

	local imageTexture = graphics.newTexture( { type = "image", filename = "glitcher/textures/rgbShift.png", baseDir=system.ResourceDirectory } )

	display.setDefault( "textureWrapX", "clampToEdge" )
	display.setDefault( "textureWrapY", "clampToEdge" )

	self.view.fill =
	{
		type = "composite",
		paint1 = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir },
		paint2 = { type = "image", filename = imageTexture.filename, baseDir = imageTexture.baseDir }
	}

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
