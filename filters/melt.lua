local kernel =
{
	category = "filter",
	group = "glitcher",
	name = "melt",
	isTimeDependent = true,
	vertexData =
	{
		{
			name = "speed",
			default = 1.5,
			min = 0.0,
			max = 100.0,
			index = 0,  -- This corresponds to "CoronaVertexUserData.x"
		},
		{
			name = "amplitude",
			default = 0.1,
			min = 0.0,
			max = 1.0,
			index = 1,  -- This corresponds to "CoronaVertexUserData.y"
		},
	},

	fragment = [[

		P_DEFAULT float rand(float n){return fract(sin(n) * 43758.5453123);}

		P_DEFAULT float noise(float p){
			P_DEFAULT float fl = floor(p);
			P_DEFAULT float fc = fract(p);
			return mix(rand(fl), rand(fl + 1.0), fc);
		}


		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ){

			P_UV vec2 uv = texCoord;

			P_DEFAULT float amp = (1. + sin(CoronaTotalTime * CoronaVertexUserData.x)) * .5 * CoronaVertexUserData.y;
			//amp = noise(CoronaTotalTime * CoronaVertexUserData.x) * CoronaVertexUserData.y;
			uv.y += noise(CoronaTotalTime + uv.x * 50.) * amp;
			uv.x += noise(CoronaTotalTime + uv.y * 25.) * amp;

			uv = clamp(uv, 0., 1.);

			P_COLOR vec4 col = texture2D(CoronaSampler0, uv);

			return CoronaColorScale(col);
		}
	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
