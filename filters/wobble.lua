local kernel =
{
	category = "filter",
	group = "glitcher",
	name = "wobble",
	isTimeDependent = true,
	vertexData =
	{
		{
			name = "speed",
			default = 10.0,
			min = 0.0,
			max = 100.0,
			index = 0,  -- This corresponds to "CoronaVertexUserData.x"
		},
		{
			name = "frequency",
			default = 25.0,
			min = 0.0,
			max = 100.0,
			index = 1,  -- This corresponds to "CoronaVertexUserData.y"
		},
		{
			name = "amplitude",
			default = 0.0030,
			min = 0.0,
			max = 1.0,
			index = 2,  -- This corresponds to "CoronaVertexUserData.z"
		},
	},

	fragment = [[

		P_UV vec2 wobble(vec2 uv, float amplitude, float frequence, float speed)
		{
			P_DEFAULT float offset = amplitude*sin(uv.y*frequence+CoronaTotalTime*speed);
			return vec2(uv.x+offset,uv.y);
		}

		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ){

			P_UV vec2 uv = texCoord;
			P_DEFAULT float amplitude = CoronaVertexUserData.z;
			P_DEFAULT float frequence = CoronaVertexUserData.y;
			P_DEFAULT float speed = CoronaVertexUserData.x;
			uv = wobble(uv,amplitude,frequence,speed);

			P_COLOR vec4 col = texture2D(CoronaSampler0,uv);

			return CoronaColorScale(col);
		}
	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
