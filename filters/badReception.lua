--https://www.shadertoy.com/view/4djczy
local kernel =
{
	category = "filter",
	group = "glitcher",
	name = "badReception",
	isTimeDependent = true,
	vertexData =
	{
		{
			name = "noise",
			default = 0.4,
			min = 0,
			max = 1.0,
			index = 0,  -- This corresponds to "CoronaVertexUserData.x"
		},
		{
			name = "rgbShift",
			default = 0.01,
			min = 0,
			max = 1.0,
			index = 1,  -- This corresponds to "CoronaVertexUserData.y"
		},
		{
			name = "ghostReflection",
			default = 0.03,
			min = 0,
			max = 1.0,
			index = 2,  -- This corresponds to "CoronaVertexUserData.z"
		},
	},

	fragment = [[

		#define bypass false

		P_DEFAULT float rand(vec2 co){
			return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
		}


		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ){

			P_UV vec2 uv = texCoord;

			P_DEFAULT float randomValue = rand(vec2(uv.x+sin(CoronaTotalTime), uv.y+cos(CoronaTotalTime)));
			P_DEFAULT float rgbShift = sin(CoronaTotalTime+randomValue)*CoronaVertexUserData.y;

			if(randomValue > 0.95-CoronaVertexUserData.z)
				uv.x+=sin(CoronaTotalTime/5.0)*0.5;

			uv.y += (cos(CoronaTotalTime*randomValue)+0.5) * (randomValue*0.01);

			P_DEFAULT float colorr = texture2D(CoronaSampler0, vec2(uv.x+rgbShift, uv.y)).r;
			P_DEFAULT float colorg = texture2D(CoronaSampler0, vec2(uv.x, uv.y)).g;
			P_DEFAULT float colorb = texture2D(CoronaSampler0, vec2(uv.x-rgbShift, uv.y)).b;

			P_UV vec4 movieColor = vec4(colorr,colorg,colorb, 1.0);
			P_UV vec4 noiseColor = vec4(randomValue,randomValue,randomValue,1.0);

			if(randomValue > 0.55-CoronaVertexUserData.z)
				noiseColor = abs(noiseColor - 0.2);

			P_COLOR vec4 col;

			if(bypass)
				col = texture2D(CoronaSampler0, texCoord);
			else
				col = mix(movieColor, noiseColor, CoronaVertexUserData.x);

			return CoronaColorScale(col);
		}

	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
