local kernel =
{
	category = "filter",
	group = "glitcher",
	name = "shudder",
	isTimeDependent = true,
	vertexData =
	{
		{
			name = "amount",
			default = 0.1,
			min = 0,
			max = 0.5,
			index = 0,  -- This corresponds to "CoronaVertexUserData.x"
		},
	},

	fragment = [[

		P_DEFAULT float sat( float t ) {
			return clamp( t, 0.0, 1.0 );
		}

		P_UV vec2 sat( vec2 t ) {
			return clamp( t, 0.0, 1.0 );
		}

		//remaps inteval [a;b] to [0;1]
		P_DEFAULT float remap  ( float t, float a, float b ) {
			return sat( (t - a) / (b - a) );
		}

		//note: /\ t=[0;0.5;1], y=[0;1;0]
		P_DEFAULT float linterp( float t ) {
			return sat( 1.0 - abs( 2.0*t - 1.0 ) );
		}

		P_UV vec3 spectrum_offset( float t ) {
			P_UV vec3 ret;
			P_DEFAULT float lo = step(t,0.5);
			P_DEFAULT float hi = 1.0-lo;
			P_DEFAULT float w = linterp( remap( t, 1.0/6.0, 5.0/6.0 ) );
			P_DEFAULT float neg_w = 1.0-w;
			ret = vec3(lo,1.0,hi) * vec3(neg_w, w, neg_w);
			return pow( ret, vec3(1.0/2.2) );
		}

		//note: [0;1]
		P_DEFAULT float rand( vec2 n ) {
			return fract(sin(dot(n.xy, vec2(12.9898, 78.233)))* 43758.5453);
		}

		//note: [-1;1]
		P_DEFAULT float srand( vec2 n ) {
			return rand(n) * 2.0 - 1.0;
		}

		P_DEFAULT float mytrunc( float x, float num_levels )
		{
			return floor(x*num_levels) / num_levels;
		}

		P_UV vec2 mytrunc( vec2 x, float num_levels )
		{
			return floor(x*num_levels) / num_levels;
		}

		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord ) {
			P_UV vec2 uv = texCoord.xy;
			uv.y = uv.y;

			P_DEFAULT float time = mod( CoronaTotalTime * 100.0, 32.0)/110.0 ;// + modelmat[0].x + modelmat[0].z;

			P_DEFAULT float GLITCH = CoronaVertexUserData.x;

			P_DEFAULT float gnm = sat( GLITCH );
			P_RANDOM float rnd0 = rand( mytrunc( vec2(time, time), 6.0 ) );
			P_DEFAULT float r0 = sat((1.0-gnm)*0.7 + rnd0);
			P_RANDOM float rnd1 = rand( vec2(mytrunc( uv.x, 10.0*r0 ), time) ); //horz
			//float r1 = 1.0f - sat( (1.0f-gnm)*0.5f + rnd1 );
			P_DEFAULT float r1 = 0.5 - 0.5 * gnm + rnd1;
			r1 = 1.0 - max( 0.0, ((r1<1.0) ? r1 : 0.9999999) ); //note: weird ass bug on old drivers
			P_RANDOM float rnd2 = rand( vec2(mytrunc( uv.y, 40.0*r1 ), time) ); //vert
			P_DEFAULT float r2 = sat( rnd2 );

			P_RANDOM float rnd3 = rand( vec2(mytrunc( uv.y, 10.0*r0 ), time) );
			P_DEFAULT float r3 = (1.0-sat(rnd3+0.8)) - 0.1;

			P_RANDOM float pxrnd = rand( uv + time );

			P_DEFAULT float ofs = 0.05 * r2 * GLITCH * ( rnd0 > 0.5 ? 1.0 : -1.0 );
			ofs += 0.5 * pxrnd * ofs;

			uv.y += 0.1 * r3 * GLITCH;

			const P_DEFAULT int NUM_SAMPLES = 20;
			const P_DEFAULT float RCP_NUM_SAMPLES_F = 1.0 / float(NUM_SAMPLES);

			P_UV vec4 sum = vec4(0.0);
			P_UV vec3 wsum = vec3(0.0);
			for( int i=0; i<NUM_SAMPLES; ++i )
			{
				P_DEFAULT float t = float(i) * RCP_NUM_SAMPLES_F;
				uv.x = sat( uv.x + ofs * t );
				P_UV vec4 samplecol = texture2D( CoronaSampler0, uv, -10.0 );
				P_UV vec3 s = spectrum_offset( t );
				samplecol.rgb = samplecol.rgb * s;
				sum += samplecol;
				wsum += s;
			}
			sum.rgb /= wsum;
			sum.a *= RCP_NUM_SAMPLES_F;

			P_COLOR vec4 col;

			col.a = sum.a;
			col.rgb = sum.rgb; // * outcol0.a;

			return CoronaColorScale( col );
		}
	]],
}

local filter = {}
filter.__index = filter

function filter:new( view, params )

	local instance = setmetatable( {}, self )

	if not kernel.defined then graphics.defineEffect( kernel ); kernel.defined = true end

	self.view = view

	-- set the view's fill using the canvas
	self.view.fill = { type = "image", filename = params.canvas.filename, baseDir = params.canvas.baseDir }

	self.view.fill.effect = kernel.category .. "." .. kernel.group .. "." .. kernel.name

	return instance

end

return filter
